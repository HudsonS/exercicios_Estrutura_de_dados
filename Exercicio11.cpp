#include <iostream>

using namespace std;

#include <iostream>

using namespace std;

const int tamanho=10;

int vetor[tamanho], val=0;


void lerVetor(int vetor[],int tamanho){
    for (int i=0;i<tamanho;i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vetor[i];
    }
}

void imprimirVetor(int vetor[],int tamanho){
    cout << endl;
    for (int i=0; i < tamanho; i++){
        cout << "Posicao "<< (i+1) <<": "<<vetor[i]<<endl;
    }
}

void bubbleSort(int vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        for (int j= tamanho-1; j>=i; j--){
            if (vetor[j-1]>vetor[j]){
                int temp = vetor[j-1];
                vetor[j-1]=vetor[j];
                vetor[j]=temp;
            }
        }
    }
}


void selectionSort(int vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        int menor = i;
        for (int j=i+1; j < tamanho; j++){
            if (vetor[j] < vetor[menor]){
                menor = j;
            }
        }
        if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }
}

void insertionSort(int vetor[],int tamanho){
    for (int i=1;i<tamanho;i++){
        int comp = vetor[i];
        int j = i - 1;
        for (;j>=0 && comp < vetor[j];j--){
            vetor[j+1]=vetor[j];
        }
        vetor[j+1] = comp;
    }
}

void escolhaOrden(int val, int tamanho){
    cout << endl << "Escolha um modo de ordena��o:" << endl;
    cout << "[1] Bubble Sort" << endl << "[2] Selection Sort" << endl << "[3] Insertion Sort" << endl;
    cin >> val;

    switch(val){
    case 1:
        cout << "Voce escolheu o metodo Bubble Sort" << endl << "ORDENACAO:";
        bubbleSort(vetor,tamanho);
        break;
    case 2:
        cout << "Voce escolheu o metodo Selection Sort" << endl << "ORDENACAO:";
        selectionSort(vetor,tamanho);
        break;
    case 3:
        cout << "Voce escolheu o metodo Insertion Sort" << endl << "ORDENACAO:";
        insertionSort(vetor,tamanho);
        break;
    default:
        cout << "Opcao invalida! Escolha outra opcao:" << endl;
        break;
        }
    if((val > 0) && (val < 4)){

    }else{
        escolhaOrden(val,tamanho);
    }
}

int main()
{
    lerVetor(vetor, tamanho);
    escolhaOrden(val, tamanho);
    imprimirVetor(vetor, tamanho);
    return 0;
}
