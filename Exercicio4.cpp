#include <iostream>

using namespace std;

const int tamanho=10;
int* vetor[tamanho];
int i, maior, menor, media, posMaior, posMenor;

void inicializarVetor(int *vetor[], int tamanho){
    for(int i=0; i<tamanho; i++){
        vetor[i]=new int;
        *(vetor[i])=0;
    }
}

void lerVetor(int *vetor[], int tamanho){
    for(int i=0; i<tamanho; i++){
        cout << "Nota " << i+1 << ": ";
        cin >> *(vetor[i]);
    }
}

void imprimirVetor(int *vetor[], int tamanho){
    cout << "VALORES DAS NOTAS" << endl;
    for(int i=0; i<tamanho; i++){
        cout << "Nota " << (i+1) <<": "<< *(vetor[i])<< endl;
    }
}

void maiorNota(int *vetor[], int tamanho){
    maior = *vetor[0];
    for(int i=1; i<tamanho; i++){
        if(*vetor[i] > maior){
            maior = *vetor[i];
            posMaior = i+1;
        }

    }
    cout << "A maior nota e " << maior << " na posicao " << posMaior;
}

void menorNota(int *vetor[], int tamanho){
    menor = *vetor[0];
    for(int i=1; i < tamanho; i++){
        if(*vetor[i] < menor){
            menor = *vetor[i];
            posMenor = i+1;
        }
    }
    cout << endl << "A menor nota e " << menor << " na posicao " << posMenor;
}

void mediaNotas(int *vetor[], int tamanho){
    int soma=0;
    for(int i=0; i<=tamanho; i++){
        soma = soma + *vetor[i];
        media = soma/tamanho;
    }
    cout << endl << "A media das nota e " << media;
}

int main()
{
    inicializarVetor(vetor,tamanho);
    lerVetor(vetor,tamanho);
    //imprimirVetor(vetor,tamanho);
    maiorNota(vetor,tamanho);
    menorNota(vetor,tamanho);
    mediaNotas(vetor,tamanho);



    return 0;
}
