#include <iostream>

using namespace std;

const int tamanho = 5;

int* numeros[tamanho];

void inicializarVetor(int *numeros[],int tam){
    for (int i=0; i < tam; i++){
        numeros[i] = new int;
        *(numeros[i])=0;
    }
}

void imprimirVetor(int *numeros[],int tam){
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": "<<*(numeros[i])<<endl;
    }
}

void lerVetor(int *numeros[],int tam){
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> *(numeros[i]);
    }
}

void bubbleSort(int *numeros[],int tamanho){
    for (int i=0; i < tamanho; i++){
        for (int j= tamanho-1; j>=i; j--){
            if (*(numeros[j-1]) >*(numeros[j])){
                int temp = *(numeros[j-1]);
                *(numeros[j-1]) = *(numeros[j]);
                *(numeros[j]) = temp;
            }
        }
    }
}

int main()
{
    inicializarVetor(numeros,tamanho);
    lerVetor(numeros,tamanho);
    bubbleSort(numeros,tamanho);
    imprimirVetor(numeros,tamanho);
    return 0;
}
