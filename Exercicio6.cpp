#include <iostream>

using namespace std;

const int tam = 15;

int* num[tam];

void inicializarVetor(int *num[],int tam){
    for (int i=0; i < tam; i++){
        num[i] = new int;
        *(num[i])=0;
    }
}

void lerVetor(int *num[],int tam){
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> *(num[i]);
    }
}

void inverterVetor(int *num[],int tam){
    int i=0, j=tam-1, temp=0;
    while(i<j){
        temp = *num[i];
        *num[i] = *num[j];
        *num[j] = temp;
        j--;
        i++;
    }
}

void imprimirVetor(int *num[],int tam){
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": "<<*(num[i])<<endl;
    }
}
int main()
{
    inicializarVetor(num,tam);
    lerVetor(num,tam);
    inverterVetor(num,tam);
    imprimirVetor(num,tam);

    return 0;
}


